package pl.edu.pjwstk.mpr.lab5.service;

import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
        List<User> usersRet = null;
        usersRet = users.stream().filter(user -> user.getPersonDetails().getAddresses().size() > 1).collect(Collectors.toList());
        return usersRet;
    }

    public static Person findOldestPerson(List<User> users) {
        return users.stream().map(user -> user.getPersonDetails()).max(Comparator.comparing(per -> per.getAge())).get();
    }

    public static User findUserWithLongestUsername(List<User> users) {
        return users.stream().max(Comparator.comparing(user -> user.getName().length())).get();
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
        return users.stream().filter(user -> user.getPersonDetails().getAge() > 18)
                .map(user -> user.getPersonDetails().getName() + " " + user.getPersonDetails().getSurname())
                .collect(joining(","));
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {

        return users.stream().filter(user -> user.getPersonDetails().getName().startsWith("A"))
                .map(p -> p.getPersonDetails().getRole().getPermissions())
                .flatMap(l -> l.stream())
                .map(permission -> permission.getName())
                .sorted((g1, g2) -> g1.compareTo(g2))
                .collect(Collectors.toList());
    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
        users.stream().filter(user -> user.getPersonDetails().getSurname().startsWith("S"))
                .map(p -> p.getPersonDetails().getRole().getPermissions())
                .flatMap(l -> l.stream())
                .map(permission -> permission.getName())
                .sorted((g1, g2) -> g1.compareTo(g2))
                .forEach(System.out::println);
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
        return users.stream().collect(Collectors.groupingBy(u -> u.getPersonDetails().getRole()));
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
        return users.stream().collect(Collectors.partitioningBy(u -> u.getPersonDetails().getAge() > 18));
    }
}
